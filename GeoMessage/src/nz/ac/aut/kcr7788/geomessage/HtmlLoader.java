package nz.ac.aut.kcr7788.geomessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import android.app.Activity;

/**
 * A Class that handles called to web pages in its own thread. The server response 
 * is passed to the HtmlLoaderListener run on the UI thread
 * 
 * @author Kerry Powell & James Fairburn
 * @version 1.0
 */
public class HtmlLoader extends Thread {
	
	private final HtmlLoaderListener listener;
	private String url;
	private String[] result;
	
	/**
	 * Create a HTML loader instance to retrieve a response from the given URL 
	 * 
	 * @param url to connect and retrieve the data from
	 * @param listener object the response <div> data is passed to
	 */
	public HtmlLoader(String url, HtmlLoaderListener listener) {
		System.out.println("URL:" + url);
		this.url = url;
		this.listener = listener;
		this.start();
	}
	
	@Override
	public void run() {
		
		InputStream is = null;
		try {
			// Create HTTP Client and connect to the url using GET request
			HttpClient httpclient = new DefaultHttpClient(); 	
			HttpGet httpget = new HttpGet(url); 				
			HttpResponse response = httpclient.execute(httpget);
			// Catch the response and load it into a buffered reader using iso-8859-1
			HttpEntity entity = response.getEntity(); 
			is = entity.getContent(); 							
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			// Create a temp result array and prepare to extract data
			ArrayList<String> array = new ArrayList<String>();	
			boolean divFound = false;
			String line = "";
			// Go through the response line by line extracting all <div> data
			while ((line = reader.readLine()) != null) {
				// Only extract data where there is a <div>
				if (line.contains("div>") || divFound) {
					// Save the line to a the temp array, remove <div> tag and white space
					divFound = !line.contains("</div>");
					line = line.replaceAll("<[/]*div>", "");
					line = line.replaceAll(" +", "");
					array.add(line);
				}
			}
			result = array.toArray(new String[array.size()]);
		} catch(IOException ex) {
			// If there is an error with making the request
			result = null;
		} finally {
			// Close the Input Stream
			if (is != null)
				try { is.close(); } catch (IOException e) {}
			// Pass the result to the listener on the UI thread
			listener.getActivity().runOnUiThread(new Runnable() {
				
                public void run() {
                	// Call the appropreate response from the listener
                	if (result != null) {
                		listener.recieveResults(result);
                	} else {
            			listener.resultError();
                	}
                }
            });
		}
	}
	
	/**
	 * Interface that allows the loader to pass the server response back
	 * 
	 * @author Kerry Powell & James Fairburn
	 * @version 1.0
	 *
	 */
	public interface HtmlLoaderListener {
		
		/**
		 * Listen for the results of this HTML Loader so that they can be passed to the method
		 * 
		 * @param result is the HTML response form the URL
		 */
		public void recieveResults(String[] result);
		
		/**
		 * Listens for an error response for connecting to the sever
		 */
		public void resultError();
		
		/**
		 * Get the activity that this response is going to run on so the results can 
		 * Run on the UI Thread
		 * 
		 * @return The activity to make changes on
		 */
		public Activity getActivity();
	}
}
