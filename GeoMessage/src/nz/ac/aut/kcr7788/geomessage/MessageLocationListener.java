package nz.ac.aut.kcr7788.geomessage;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

/**
 * A Class that handles updating the current location of the user via GPS and Wifi
 * It runs on its own thread in the background
 * 
 * @author Kerry Powell and James Fairburn
 * @version 1.0
 */
public class MessageLocationListener extends Thread implements LocationListener{
	
	public static double longitude;
	public static double latitude;
	public static String provider;
	private boolean locAvaiable = true;
	private int checkedCount = 0;
    private static LocationManager locationManager;
    
    /**
     * Create a message activity 
     * 
     * @param activity
     */
    public MessageLocationListener(Activity activity){
    	longitude = 174.764183;
		latitude = -36.850727;
    	provider = locationManager.GPS_PROVIDER;
    	locationManager = (LocationManager)activity.getSystemService(Context.LOCATION_SERVICE); 
    	locationManager.requestLocationUpdates(provider, 0, 0, this);
    	start();
    }
    
	@Override
	public void onLocationChanged(Location location) {
		// Set the long and lat Static details
		longitude = location.getLongitude();
		latitude = location.getLatitude();
		if(provider.equals("none")){
			longitude = 174.764183;
			latitude = -36.850727;
		}
	}
	
	@Override
	public void run() {
		
		while(true){
			// Sleep this thread every 2000 milliseconds
			try { Thread.sleep(2000); } catch (InterruptedException e) {}
			if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && locAvaiable) {
				// Set the provider as GPS
				provider = LocationManager.GPS_PROVIDER;
				locationManager.requestLocationUpdates(provider, 0, 0, this);
				//Is GPS actually getting a location
				if (longitude == 0.0) {
					// If this fails 4 times, change provider
					checkedCount++;
					if(checkedCount == 4){
						locAvaiable = false;
					}
				}
			} else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
				// Set the provider as WIFI
				provider = LocationManager.NETWORK_PROVIDER;
				locationManager.requestLocationUpdates(provider, 0, 0, this);
			} else {
				
				provider = "none";
			}
		}
	}
	
	@Override
	public void onProviderDisabled(String provider) {}

	@Override
	public void onProviderEnabled(String provider) {}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {}
}
