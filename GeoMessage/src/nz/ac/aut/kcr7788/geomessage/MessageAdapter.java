package nz.ac.aut.kcr7788.geomessage;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import nz.ac.aut.kcr7788.geomessage.HtmlLoader.HtmlLoaderListener;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * An adapter that handles the storage of all messages and responses form the server
 * 
 * @author Kerry Powell & James Fairburn
 * @version 1.0
 */
public class MessageAdapter extends ArrayAdapter<String> {
	
    private List<String> messages;
    private List<String> userNames = new ArrayList<String>();
    private LayoutInflater inflater;
    private int layoutRecieved = R.layout.adapter_message_received;
    private int layoutSent = R.layout.adapter_message_sent;
    private int messageId = R.id.message;
    private int userNameId = R.id.userName;
    private String myUserName;
    private Activity activity;
    private long maxId = 0;
	private String addUrl = "AddMessage?";
	private String getUrl = "GetMessages?";
 	private String urlName = "name=";
	private String urlMaxId = "&maxId=";
	private String urlLong = "&longitude=";
	private String urlLat = "&latitude=";
	private String urlMsg = "&msg=";
	private MessageUpdate updater;

	/**
	 * Create a message adapter to handle all message data. This will vreate an 
	 * 
	 * @param activity that this list is in
	 * @param userName of the user that created this
	 * @param maxId the last message ID that was received
	 * @param messages sent to the server
	 */
    public MessageAdapter(Activity activity, String userName, long maxId, List<String> messages) {
    	
    	super(activity, R.layout.adapter_message_received, messages);
    	this.messages = messages;
    	this.activity = activity;
    	this.myUserName = userName;
    	this.maxId = maxId;
    	updater = new MessageUpdate();
    	updater.start();
    	inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    
    /**
     * Stop requesting messages from server
     */
    public void stopMessageUpdate() {
    	updater.stopUpdating();
    }
    
    /**
     * Start Requesting messages from the server
     */
    public void startMessageUpdate() {
    	if (!updater.isAlive())
    		updater.start();
    }
    
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	
    	View rowView = null;
        if (userNames.get(position).equals(myUserName)) {
        	
        	rowView = inflater.inflate(layoutSent, parent, false);
        } else {
        	
        	rowView = inflater.inflate(layoutRecieved, parent, false);
	        TextView userNameView = (TextView)rowView.findViewById(userNameId);
	        userNameView.setText(userNames.get(position));
        }
        TextView messageView = (TextView)rowView.findViewById(messageId);
        messageView.setText(messages.get(position));
        return rowView;
    }

    @Override
    public boolean hasStableIds() {
    	
    	return true;
    }
    
    /**
     * Adds many messages to the message adapter
     * 
     * @param messages array to add
     * @param userNames array to add
     */
    public synchronized void addMessages(String[] messages, String[] userNames) {
    	
    	for (int i = 0; i < messages.length && i < userNames.length; i++) {
    		this.messages.add(messages[i]);
    		this.userNames.add(userNames[i]);
    	}
    	notifyDataSetChanged();
    }
    
    /**
     * Add a single message to the adapter and send it to the server
     * to be added
     * 
     * @param message to add to the adapter
     */
    public synchronized void sendMessage(String message) {
    	
    	messages.add(message);
    	userNames.add(myUserName);
    	sendMessageToServer(message);
        notifyDataSetChanged();
    }
    
    /**
     * Sends the message data to the server with the users name
     * 
     * @param message
     */
    private void sendMessageToServer(String message) {
    	//Encode the message to be sent via GET request
    	message = URLEncoder.encode(message);
    	System.err.println("Message is:" + message);
    	String url = LoginActivity.getUrl() + addUrl + 
    			urlName + myUserName + 
    			urlLong + MessageLocationListener.longitude + 
    			urlLat + MessageLocationListener.latitude + 
    			urlMsg + message; 
    	new HtmlLoader(url, new MessageSent());
    }
    
    /**
     * Updates the max ID number
     * 
     * @param id the new ID value
     */
    private synchronized void setLastId(long id) {
    	
    	if (maxId < id) 
    		maxId = id;
    }
    
    /**
     * Get the current max ID
     * 
     * @return the last message ID
     */
    private synchronized long getLastId() {
    	return maxId;
    }
    
    /**
     * Class to handle the response from sending a message to the server
     * 
     * @author Kerry Powell & James Fairburn
     * @version 1.0
     */
    private class MessageSent implements HtmlLoaderListener{

		@Override
		public void recieveResults(String[] result) {
			// Set the default message
			String toast = "Message not sent";
			if (result.length > 0) {
				
				if (result[0].equals("Success")) {
					// If there is a successful response from the server
					toast = "Message sent";
				} else {
					// If there is an error
					toast += " \n result: " + result[0];
				}
			}
			Toast.makeText(activity, toast, Toast.LENGTH_SHORT).show();
		}
	
		@Override
		public void resultError() {
			Toast.makeText(activity, "Failed to connect to server", Toast.LENGTH_SHORT).show();
		}
	
		@Override
		public Activity getActivity() {
			return activity;
		}
    }
    
    /**
     * A class to handle the sending the request for new messages and getting the response
     * from the server. This will add those messages to the list then request more messages 
     * from the server
     * 
     * @author Kerry Powell & James Fairburn
     * @version 1.0
     */
    private class MessageUpdate extends Thread implements HtmlLoaderListener{

    	private final int REQUEST_PASS = 5;
    	private final int DELAY = 10000;
    	private int shouldRequest = REQUEST_PASS;
    	private boolean keepRunning = true;
    	
    	@Override
    	public void run() {
    		//Set the defaults
    		keepRunning = true;
    		shouldRequest = REQUEST_PASS;
    		while (keepRunning) {
    			//Delay the update process for 10sec
				try { Thread.sleep(DELAY); } catch (InterruptedException e) {}
				if (++shouldRequest > REQUEST_PASS) {
					//Send a request to the server for more new messages
					shouldRequest = 0;
					String url = LoginActivity.getUrl() + getUrl + 
							urlName + myUserName + 
							urlLong + MessageLocationListener.longitude + 
							urlLat + MessageLocationListener.latitude +
							urlMaxId + maxId;
					new HtmlLoader(url, this);
				}
    		}
    	}
    	
    	/**
    	 * Stop requesting updates
    	 */
    	public void stopUpdating() {
    		
    		keepRunning = false;
    	}
    	
		@Override
		public void recieveResults(String[] result) {
			
			if (result.length > 1) {
				// If data was received from the server
				setLastId(Long.parseLong(result[0]));
				int count = Integer.parseInt(result[1]);
				// Pass the new messages to the list
				String[] messages = new String[count];
				String[] userNames = new String[count];
				for (int i = 0; i < count; i++) {
					
					userNames[i] = result [(i*2)+2];
					messages[i] = result [(i*2)+3];
				}
				addMessages(messages, userNames);
			}
			// Allow the requester to make another request
			shouldRequest = REQUEST_PASS;
		}
	
		@Override
		public void resultError() {
			// Tell the user there was an error making a request to the server
			shouldRequest = REQUEST_PASS;
			Toast.makeText(activity, "Failed to connect to server", Toast.LENGTH_SHORT).show();
		}
	
		@Override
		public Activity getActivity() {
			
			return activity;
		}
    }
}
