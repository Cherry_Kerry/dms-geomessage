package nz.ac.aut.kcr7788.geomessage;

import nz.ac.aut.kcr7788.geomessage.HtmlLoader.HtmlLoaderListener;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * The activity that handles the user logging into the service. Here the user picks
 * a user name which is checked with the server
 * 
 * @author Kerry Powell & James Fairburn
 * @version 1.0
 */
public class LoginActivity extends Activity implements HtmlLoaderListener {
	
	private static String serverUrlWeb = "http://ngatiawa2.asuscomm.com:8080/GeoMessageServer/";
	private static String serverUrlLocal = "http://10.0.2.2:8080/GeoMessageServer/";
	private static String serverUrl = serverUrlLocal;
	private String loginUrl = "Login?";
	private String loginUrlName = "name=";
	private String loginUrlId = "&aId=";
	private boolean isLoggingIn = false;
	private EditText loginFeild = ((EditText)findViewById(R.id.loginField));
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Prepare activity
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		new MessageLocationListener(this);
	}
	
	/**
	 * Get the server URL that GeoMessage logged into
	 * 
	 * @return the URL string for the server
	 */
	public static String getUrl() {
		return serverUrl;
	}
	
	/**
	 * The login action triggered by the user clicking the Login button
	 * 
	 * @param view
	 */
	public void loginAction(View view) {
		//If button has not been clicked to login
		if (canLogIn()) {
			try {
				// Get the login details
				String loginName = getLoginName();
				String deviceId = getDeviceId();
				// Initiate logging in with the server,
				Toast.makeText(this, "Logging In, Please wait", Toast.LENGTH_LONG).show();
				loginUser(loginName, deviceId);
			} catch (IllegalArgumentException ex) {
				// Notify the user there is an issue with their user name
				Toast.makeText(this, "Please enter a valid login name", Toast.LENGTH_SHORT).show();
				finishedLogginIn();
			}
		}
	}
	
	/**
	 * Unblock the user from logging it
	 */
	private synchronized void finishedLogginIn() {
		
		isLoggingIn = false;
		loginFeild.setEnabled(true);
	}
	
	/**
	 * Method to stop the user form trying to login multiple times on accident
	 * 
	 * @return if the user is allowed to log in
	 */
	private synchronized boolean canLogIn(){
		
		if (isLoggingIn == false) {
			
			isLoggingIn = true;
			loginFeild.setEnabled(false);
			return true;
		}
		return false;
	}
	
	/**
	 * Get the login name of the user from the input feild
	 * 
	 * @return the users selected login name
	 * @throws IllegalArgumentException if there is an issue with the format of the user name
	 */
	private String getLoginName() throws IllegalArgumentException {
		
		String regEx = "[A-Za-z0-9_]{5,16}";
		String loginName = loginFeild.getText().toString();
		
		if (loginName.matches(regEx))
			return loginName;
		
		throw new IllegalArgumentException();
	}
	
	/**
	 * Get the Android Device ID for individual identification to allow users
	 * to reuse a user name they have already selected
	 * 
	 * @return the Android Device ID
	 */
	private String getDeviceId() {
		
		String id = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID); 
		if (id == null)
			id = "AndroidNoID";
		return id;
	}
	
	/**
	 * Invoke the html request to login the user
	 * 
	 * @param loginName
	 * @param deviceId
	 */
	private void loginUser(String loginName, String deviceId) {
		
		String url = serverUrl + loginUrl + loginUrlName + loginName + loginUrlId + deviceId;
		new HtmlLoader(url, this);
	}
	
	/**
	 * Load the message activity 
	 * 
	 * @param maxId the max message id number from the server login response
	 */
	private void proceedToMessageActivity(Long maxId) {
		
		try {
			// Create the login intent with the user name and max id
			Intent messageActivity = new Intent(this, MessageActivity.class);
			messageActivity.putExtra(MessageActivity.INTENT_LOGIN_NAME, getLoginName());
			messageActivity.putExtra(MessageActivity.INTENT_MAX_ID, maxId);				
			startActivity(messageActivity);
		} catch (IllegalArgumentException ex) {
			// If there is an error creating the activity
			finishedLogginIn();
			Toast.makeText(this, "Failed to login", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void recieveResults(String[] result) {

		if (result.length > 0) {
			
			if (result[0].equals("Accept") && result.length > 1) {
				// If the response form the server is accept, login
				proceedToMessageActivity(Long.parseLong(result[1]));
			} else if (result[0].equals("UserTaken")) {
				// If the response says the user is taken
				Toast.makeText(this, "This login name has been taken", Toast.LENGTH_SHORT).show();
				finishedLogginIn();
			} else {
				// If the response says there is an issue with the user name
				Toast.makeText(this, "Please enter a valid username", Toast.LENGTH_SHORT).show();
				finishedLogginIn();
			}
		} else {
			// If the response is empty
			resultError();
		}
		
	}

	@Override
	public Activity getActivity() {
		
		return this;
	}

	@Override
	public void resultError() {
		
		finishedLogginIn();
		if (serverUrl.equals(serverUrlLocal)) {
			// Swap the URL to connect to our server if the local host 10.0.2.2 is not found
			serverUrl = serverUrlWeb;
			loginAction(null);
		} else {
			
			Toast.makeText(this, "Failed to connect to server", Toast.LENGTH_SHORT).show();
		}
	}
}
