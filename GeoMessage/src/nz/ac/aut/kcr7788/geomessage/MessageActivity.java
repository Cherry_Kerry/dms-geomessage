package nz.ac.aut.kcr7788.geomessage;

import java.util.ArrayList;
import android.os.Bundle;
import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

/**
 * The activity for managing and handling the message layout and user sending messages
 * 
 * @author Kerry Powell & James Fairburn
 *
 */
public class MessageActivity extends Activity {

	public final static String INTENT_LOGIN_NAME = "loginName";
	public final static String INTENT_MAX_ID = "maxID";
	private String myLoginName;
	private MessageAdapter adapter;
	private EditText messageText = (EditText)findViewById(R.id.messageText);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Setup the activity
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message);
		// Get the parameter data from the intent
		myLoginName = getIntent().getStringExtra(INTENT_LOGIN_NAME);
		long maxId = getIntent().getLongExtra(INTENT_MAX_ID, 0);
		// Create the message adapter to load and store all the messages
		adapter = new MessageAdapter(this, myLoginName, maxId, new ArrayList<String>());
		ListView listView = (ListView)findViewById(R.id.messageView);
		listView.setAdapter(adapter);
		
		messageText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {
				textChanged();
			}
		});
		
	}
	
	@Override 
	protected void onStart() {
		super.onStart();
		adapter.startMessageUpdate();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		adapter.stopMessageUpdate();
	}

	/**
	 * Action invoked when the text is changed, if the text become too long it
	 * automatically sends the message t the server
	 */
	protected synchronized void textChanged() {
		
		String message = getMessage();
		if(message.length() > 255) {
			sendAction(null);
		}
	}
	
	/**
	 * Get the message text from the input field
	 * 
	 * @return the message string
	 */
	private String getMessage() {
		return messageText.getText().toString();
	}
	
	/**
	 * Send the message to the server and add it to the messages list. This
	 * message is invoked by the button and textChanged
	 * 
	 * @param view that invoked the action
	 */
	public synchronized void sendAction(View view) {
		
		String message = getMessage();
		if (message.length() > 0) {
		
			messageText.setText("");
			adapter.sendMessage(message);
		}
	}
}
